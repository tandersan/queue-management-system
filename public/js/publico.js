// Variables
const socketClient = io();
const lblTicket1 = document.querySelector('#lblTicket1');
const lblEscritorio1 = document.querySelector('#lblEscritorio1');
const lblTicket2 = document.querySelector('#lblTicket2');
const lblEscritorio2 = document.querySelector('#lblEscritorio2');
const lblTicket3 = document.querySelector('#lblTicket3');
const lblEscritorio3 = document.querySelector('#lblEscritorio3');
const lblTicket4 = document.querySelector('#lblTicket4');
const lblEscritorio4 = document.querySelector('#lblEscritorio4');

// MAIN
// When Server is On - then the Create Button will be Enabled
socketClient.on('connect', () => {
});

// When Server is Off - then the Create Button will be Disabled
socketClient.on('disconnect', () => {
});

// Showing last created ticket on Create screen
socketClient.on('estado-actual', (ticketsEnPantalla) => {
    // Playing audio every time a customer is called
    const audio = new Audio('./audio/new-ticket.mp3');
    audio.play();
 
    // Note: if process.env.NUMBERTICKETSCREEN changes - this must change too
    const [ticket1, ticket2, ticket3, ticket4 ] = ticketsEnPantalla;

    if (ticket1) {
        lblTicket1.innerText = 'Ticket ' + ticket1.numero;
        lblEscritorio1.innerText = ticket1.modulo;
    }

    if (ticket2) {
        lblTicket2.innerText = 'Ticket ' + ticket2.numero;
        lblEscritorio2.innerText = ticket2.modulo;
    }

    if (ticket3) {
        lblTicket3.innerText = 'Ticket ' + ticket3.numero;
        lblEscritorio3.innerText = ticket3.modulo;
    }

    if (ticket4) {
        lblTicket4.innerText = 'Ticket ' + ticket4.numero;
        lblEscritorio4.innerText = ticket4.modulo;
    }
});