// Variables
const socketClient = io();
const lblNuevoTicket = document.querySelector('#lblNuevoTicket');
const btnGeneraTicket = document.querySelector('#btnGeneraTicket');

// MAIN
// When Server is On - then the Create Button will be Enabled
socketClient.on('connect', () => {
    btnGeneraTicket.disabled = false;
});

// When Server is Off - then the Create Button will be Disabled
socketClient.on('disconnect', () => {
    btnGeneraTicket.disabled = true;
});

// Showing last created ticket on Create screen
socketClient.on('ultimo-ticket', (ultimoTicket) => {
    lblNuevoTicket.innerText = 'Ticket ' + ultimoTicket;
});

// Loading in the ticket screen the name of ticket created
btnGeneraTicket.addEventListener('click', () => {
    socketClient.emit('siguiente-ticket', null, (ticket) => {
        lblNuevoTicket.innerText = ticket;
    });
});